#include "RocksClient.h"

#include "TextureManager.h"
#include "GameObjectRegistry.h"

#include "InputMemoryBitStream.h"
#include "OutputMemoryBitStream.h"
#include "NetworkManagerClient.h"

#include "StringUtils.h"


/**
 * The constructor for the rock in the client side
 */
RocksClient::RocksClient()
{
	mSpriteComponent.reset(new SpriteComponent(this));
	mSpriteComponent->SetTexture(TextureManager::sInstance->GetTexture("rock"));

}

/**
 * The rock is a statick object so no need for an update in it
 */
void RocksClient::Update()
{
	//no need for rock to be updated since its a static object.
}

/**
 * The same function as the player however for the rock we dont need as many cause the rock is a static object
 */
void RocksClient::Read(InputMemoryBitStream& inInputStream)
{

	bool stateBit;

	uint32_t readState = 0;

	inInputStream.Read(stateBit);	

	float replicatedRotation;
	Vector3 replicatedLocation;
	Vector3 replicatedVelocity;

	if (stateBit)
	{
		inInputStream.Read(replicatedLocation.mX);
		inInputStream.Read(replicatedLocation.mY);

		SetLocation(replicatedLocation);

		inInputStream.Read(replicatedRotation);
		SetRotation(replicatedRotation);

		readState |= ECRS_Pose;
	}


}