#include "InputManager.h"
#include <iostream>
#include "NetworkManagerClient.h"

using std::cout;
using std::endl;

unique_ptr< InputManager >	InputManager::sInstance;

namespace
{
	float kTimeBetweenInputSamples = 0.03f;
}

void InputManager::StaticInit()
{
	sInstance.reset( new InputManager() );
}


namespace
{
	inline void UpdateDesireVariableFromKey( EInputAction inInputAction, bool& ioVariable )
	{
		if( inInputAction == EIA_Pressed )
		{
			ioVariable = true;
		}
		else if( inInputAction == EIA_Released )
		{
			ioVariable = false;
		}
	}

	inline void UpdateDesireFloatFromKey( EInputAction inInputAction, float& ioVariable )
	{
		if( inInputAction == EIA_Pressed )
		{
			ioVariable = 1.f;
		}
		else if( inInputAction == EIA_Released )
		{
			ioVariable = 0.f;
		}
	}
}

void InputManager::HandleInput( EInputAction inInputAction, int inKeyCode )
{
	switch( inKeyCode )
	{
	case 'a':
		UpdateDesireFloatFromKey( inInputAction, mCurrentState.mDesiredLeftAmount );
		std::cout << "Moving left" << std::endl;
		break;
	case 'd':
		UpdateDesireFloatFromKey( inInputAction, mCurrentState.mDesiredRightAmount );
		std::cout << "Moving right" << std::endl;
		break;
	case 'w':
		UpdateDesireFloatFromKey( inInputAction, mCurrentState.mDesiredForwardAmount );
		std::cout << "Moving Forward" << std::endl;
		break;
	case 's':
		UpdateDesireFloatFromKey( inInputAction, mCurrentState.mDesiredBackAmount );
		std::cout << "Moving Backwards" << std::endl;
		break;
	case 'k':
		UpdateDesireVariableFromKey( inInputAction, mCurrentState.mIsShooting );
		std::cout << "Bullet shot!" << std::endl;

		break;
	case '+':
	case '=':
		{
			float latency = NetworkManagerClient::sInstance->GetSimulatedLatency();
			latency += 0.1f;
			if( latency > 0.5f )
			{
				latency = 0.5f;
			}
			NetworkManagerClient::sInstance->SetSimulatedLatency( latency );
			break;
		}
	case '-':
		{
			float latency = NetworkManagerClient::sInstance->GetSimulatedLatency();
			latency -= 0.1f;
			if( latency < 0.0f )
			{
				latency = 0.0f;
			}
			NetworkManagerClient::sInstance->SetSimulatedLatency( latency );
			break;
		}
	}

}


InputManager::InputManager() :
	mNextTimeToSampleInput( 0.f ),
	mPendingMove( nullptr )
{

}

const Move& InputManager::SampleInputAsMove()
{
	return mMoveList.AddMove( GetState(), Timing::sInstance.GetFrameStartTime() );
}

bool InputManager::IsTimeToSampleInput()
{
	float time = Timing::sInstance.GetFrameStartTime();
	if( time > mNextTimeToSampleInput )
	{
		mNextTimeToSampleInput = mNextTimeToSampleInput + kTimeBetweenInputSamples;
		return true;
	}

	return false;
}

void InputManager::Update()
{
	if( IsTimeToSampleInput() )
	{
		mPendingMove = &SampleInputAsMove();
	}
}
