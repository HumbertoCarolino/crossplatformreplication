#pragma once

#include "Rocks.h"
#include "SpriteComponent.h"

class RocksClient : public Rocks
{
public:
	static GameObjectPtr StaticCreate() { return GameObjectPtr(new RocksClient()); }

	virtual void Update();

	virtual void Read(InputMemoryBitStream& inInputStream) override;

protected:
	RocksClient();

private:


	SpriteComponentPtr mSpriteComponent;



};

typedef shared_ptr<RocksClient> RocksClientPtr;
