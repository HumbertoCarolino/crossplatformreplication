#include "World.h"


std::unique_ptr< World >	World::sInstance;

void World::StaticInit()
{
	sInstance.reset( new World() );
}

World::World()
{
}

/**
 * Adding game objects to the world although most of the time we use spawn functions
 */
void World::AddGameObject( GameObjectPtr inGameObject )
{
	mGameObjects.push_back( inGameObject );
	inGameObject->SetIndexInWorld( mGameObjects.size() - 1 );
}

int World::GetGameObjectCount()
{
	return mGameObjects.size();
}

/**
 * As the name suggests  removing gameobjects.
 */
void World::RemoveGameObject( GameObjectPtr inGameObject )
{
	int index = inGameObject->GetIndexInWorld();

	int lastIndex = mGameObjects.size() - 1;
	if( index != lastIndex )
	{
		mGameObjects[ index ] = mGameObjects[ lastIndex ];
		mGameObjects[ index ]->SetIndexInWorld( index );
	}

	inGameObject->SetIndexInWorld( -1 );

	mGameObjects.pop_back();
}

/**
 * update for the game world which also handles objects dying in the world.
 */
void World::Update()
{
	//update all game objects- sometimes they want to die, so we need to tread carefully...

	for( int i = 0, c = mGameObjects.size(); i < c; ++i )
	{
		GameObjectPtr go = mGameObjects[ i ];


		if( !go->DoesWantToDie() )
		{
			go->Update();
		}
		//you might suddenly want to die after your update, so check again
		if( go->DoesWantToDie() )
		{
			RemoveGameObject( go );
			go->HandleDying();
			--i;
			--c;
		}
	}
}
/**
 * Making an array of Rocks that are within our world this way we can keep track of all the walls. 
 */
void World::populatewalls()
{
	for (auto goIt = World::sInstance->GetGameObjects().begin(), end = World::sInstance->GetGameObjects().end(); goIt != end; ++goIt) 
	{
		GameObject* target = goIt->get();

		Rock* otherrock = dynamic_cast<Rock*>(target);
		if (otherrock) {
			rocklist.push_back(otherrock);
		}


	}

}

void World::populateRocks()
{

	for (auto goIt = World::sInstance->GetGameObjects().begin(), end = World::sInstance->GetGameObjects().end(); goIt != end; ++goIt)
	{
		GameObject* target = goIt->get();

		Rocks* otherrock = dynamic_cast<Rocks*>(target);
		if (otherrock) {
			rockslist.push_back(otherrock);

		}


	}



}
