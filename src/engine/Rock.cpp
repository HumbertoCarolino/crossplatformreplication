
#include "Rock.h"
#include "SDL.h"

#include "OutputMemoryBitStream.h"

/**
 * ... text ...
 */
Rock::Rock() :
	GameObject(),
	mRockId(0)
{
	//setting the rock within the work, changed for somewhere else 
	SetLocation(Vector3(.9f, .1f, .7f)); 
	SetCollisionRadius(0.2f);
	SetScale(0.05);
}

void Rock::Update()
{

}


void Rock::SetSpawnPosition()
{
	SetLocation(GetLocation());
}

void Rock::ProcessCollisions()
{

}

/**
 * ... text ...
 */
void Rock::ProcessCollisionsWithScreenWalls()
{

}
/**
 * ... text ...
 */
uint32_t Rock::Write(OutputMemoryBitStream& inOutputStream, uint32_t indirtystate) const
{


	uint32_t rockmemory = 0;

	if (indirtystate & ECRS_Pose  ) 
	{
		inOutputStream.Write((bool)true);
		Vector3 location = GetLocation();
		inOutputStream.Write(location.mX);
		inOutputStream.Write(location.mY);

		inOutputStream.Write(GetRotation());

		rockmemory |= ECRS_Pose;

	}

	return rockmemory;
}