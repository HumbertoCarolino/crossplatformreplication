#pragma once

#include <memory>
#include <math.h>
#include "GameObject.h"
#include "OutputMemoryBitStream.h"


class Rocks : public GameObject
{
public:

	CLASS_IDENTIFICATION('RCKS', GameObject)

		enum ERockReplicationState
	{
		ECRS_Pose = 1 << 0,
		ECRS_Color = 1 << 1,
		

		ECRS_AllState = ECRS_Pose | ECRS_Color
	};

		static GameObject* StaticCreate() { return new Rocks(); }

	static GameObjectPtr StaticCreatePtr() { return GameObjectPtr(new Rocks()); }

	virtual void Update() override;

	
	void SetSpawnPosition();

	void ProcessCollisions();
	void ProcessCollisionsWithScreenWalls();

	// RockID should be needed for multiple rocks/walls maybe
	void SetRockId(uint32_t inRockId) { mRockId = inRockId; }


	uint32_t GetRockId() const { return mRockId; }

	//Writing Memory
	virtual uint32_t Write(OutputMemoryBitStream& inOutputStream, uint32_t indirtystate ) const override;
	




protected:
	Rocks();

private:

	uint32_t mRockId;

};

typedef shared_ptr< Rocks > RocksPtr;

