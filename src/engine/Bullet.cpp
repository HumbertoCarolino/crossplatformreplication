#include "Bullet.h"
#include "Maths.h"
#include "InputState.h"
#include "InputMemoryBitStream.h"
#include "OutputMemoryBitStream.h"
#include "Player.h"
#include "Rock.h"
#include "Rocks.h"
#include <iostream>

using std::cout;
using std::endl;

//zoom hardcoded at 100...if we want to lock players on screen, this could be calculated from zoom
const float HALF_WORLD_HEIGHT = 3.6f;
const float HALF_WORLD_WIDTH = 6.4f;
/**
 * The Constructor for our bullet as we need to define every variable
 */
Bullet::Bullet() :
	GameObject(),
	mVelocity( Vector3::Zero ),
	mMaxLinearSpeed( 50.f ),
	mMaxRotationSpeed( 5.f ),
	mWallRestitution( 0.1f ),
	mNPCRestitution( 0.1f ),
	mBulletId( 0 ),
	mLastMoveTimestamp ( 0.0f ),
	mThrustDir( 0.f ),
	mHealth( 10 ),
	mIsShooting( false )

{
	SetCollisionRadius( 0.5f );

}

void Bullet::ProcessInput( float inDeltaTime, const InputState& inInputState )
{
	////process our input....

	////turning...
	//float newRotation = GetRotation() + inInputState.GetDesiredHorizontalDelta() * mMaxRotationSpeed * inDeltaTime;
	//SetRotation( newRotation );

	////moving...
	//float inputForwardDelta = inInputState.GetDesiredVerticalDelta();
	//mThrustDir = inputForwardDelta;


	//mIsShooting = inInputState.IsShooting();

}
/**
 * in here we are adjusting the speed which the bullet will travel at so the higher the delta time the faster the bullet is
 */
void Bullet::AdjustVelocityByThrust( float inDeltaTime )
{
	//just set the velocity based on the thrust direction -- no thrust will lead to 0 velocity
	//simulating acceleration makes the client prediction a bit more complex
	Vector3 forwardVector = GetForwardVector();
	mVelocity = forwardVector * ( bulletVelocity *  inDeltaTime * mMaxLinearSpeed );


}
/**
 * As the name suggests in here we are calling functions to "simulate Movement" for our bullet
 */
void Bullet::SimulateMovement( float inDeltaTime )
{
	//simulate us...
	AdjustVelocityByThrust( inDeltaTime );

	SetLocation( GetLocation() + mVelocity * inDeltaTime );

	ProcessCollisions();
}

void Bullet::Update()
{


}
/**
 * Collisions for our bullets cause we want them to collide with the walls of our world.
 */
void Bullet::ProcessCollisions()
{
	float sourceRadius = GetCollisionRadius();
	Vector3 sourceLocation = GetLocation();
	int rocksCount = rocksvec.size();
	int rockCount = rockvec.size();
	for (int i = 0; i < rockCount; ++i)
	{
		
		Vector3 targetLocation = rockvec[i]->GetLocation();
		


		float targetRadius = rockvec[i]->GetCollisionRadius();

		Vector3 delta = targetLocation - sourceLocation;
		float distSq = delta.Length2D();
		float collisionDist = (sourceRadius + targetRadius);


		if (distSq < (collisionDist * collisionDist))
		{
			SetDoesWantToDie(true);
		}

	}
	for (int i = 0; i < rocksCount; ++i)
	{

		Vector3 targetLocation = rocksvec[i]->GetLocation();



		float targetRadius = rocksvec[i]->GetCollisionRadius();

		Vector3 delta = targetLocation - sourceLocation;
		float distSq = delta.Length2D();
		float collisionDist = (sourceRadius + targetRadius);


		if (distSq < (collisionDist * collisionDist))
		{
			SetDoesWantToDie(true);
		}

	}
}

/**
 * Just like the player we want to collide with bullets however we want the bullets to dissapear on impact.
 */
void Bullet::ProcessCollisionsWithScreenWalls()
{

	/*RockPtr otherrock;
	otherrock->GetRockId();
	Vector3 rockplace = otherrock->GetLocation();*/
		
	
	Vector3 location = GetLocation();
	float x = location.mX;
	float y = location.mY;
	Vector3 wallslocation;


	float vx = mVelocity.mX;
	float vy = mVelocity.mY;

	float radius = GetCollisionRadius();

	//if the cat collides against a wall, the quick solution is to push it off
	if( ( y + radius ) >= HALF_WORLD_HEIGHT && vy > 0 )
	{
		

		SetDoesWantToDie(true);
		mVelocity.mY = -vy * mWallRestitution;
		location.mY = HALF_WORLD_HEIGHT - radius;
		SetLocation( location );
	}
	else if( y <= ( -HALF_WORLD_HEIGHT - radius ) && vy < 0 )
	{
		SetDoesWantToDie(true);
		mVelocity.mY = -vy * mWallRestitution;
		location.mY = -HALF_WORLD_HEIGHT - radius;
		SetLocation( location );
	}

	if( ( x + radius ) >= HALF_WORLD_WIDTH && vx > 0 )
	{
		SetDoesWantToDie(true);
		mVelocity.mX = -vx * mWallRestitution;
		location.mX = HALF_WORLD_WIDTH - radius;
		SetLocation( location );
	}
	else if(  x <= ( -HALF_WORLD_WIDTH - radius ) && vx < 0 )
	{
		SetDoesWantToDie(true);
		mVelocity.mX = -vx * mWallRestitution;
		location.mX = -HALF_WORLD_WIDTH - radius;
		SetLocation( location );
	}/*if (x == rockplace.mX && y == rockplace.mY)
	{
		SetDoesWantToDie(true);
		SetLocation(location);
	}*/
}
/**
 * This is the construtor for our bullet which it will set its position and movement speed.
 */
void Bullet::BulletConstructor(Player* PlayerPos)
{
	//Physics for the bullet on pos,rotation and place to shoot from
	SetLocation(PlayerPos->GetLocation() + (PlayerPos->GetForwardVector() * 1.02));
	SetRotation(PlayerPos->GetRotation());
	SetVelocity(PlayerPos->GetForwardVector() * bulletVelocity);

	getWalls();
}
/**
 * This is a Reimplementation from player. 
 */
uint32_t Bullet::Write( OutputMemoryBitStream& inOutputStream, uint32_t inDirtyState ) const
{
	uint32_t writtenState = 0;

	if( inDirtyState & ECRS_PlayerId )
	{
		inOutputStream.Write( (bool)true );
		inOutputStream.Write( GetBulletId() );

		writtenState |= ECRS_PlayerId;
	}
	else
	{
		inOutputStream.Write( (bool)false );
	}


	if( inDirtyState & ECRS_Pose )
	{
		inOutputStream.Write( (bool)true );

		Vector3 velocity = mVelocity;
		inOutputStream.Write( velocity.mX );
		inOutputStream.Write( velocity.mY );

		Vector3 location = GetLocation();
		inOutputStream.Write( location.mX );
		inOutputStream.Write( location.mY );

		inOutputStream.Write( GetRotation() );

		writtenState |= ECRS_Pose;
	}
	else
	{
		inOutputStream.Write( (bool)false );
	}

	//always write mThrustDir- it's just two bits
	if( mThrustDir != 0.f )
	{
		inOutputStream.Write( true );
		inOutputStream.Write( mThrustDir > 0.f );
	}
	else
	{
		inOutputStream.Write( false );
	}

	if( inDirtyState & ECRS_Color )
	{
		inOutputStream.Write( (bool)true );
		inOutputStream.Write( GetColor() );

		writtenState |= ECRS_Color;
	}
	else
	{
		inOutputStream.Write( (bool)false );
	}

	if( inDirtyState & ECRS_Health )
	{
		inOutputStream.Write( (bool)true );
		inOutputStream.Write( mHealth, 4 );

		writtenState |= ECRS_Health;
	}
	else
	{
		inOutputStream.Write( (bool)false );
	}

	//cout << "Spawn Index: " << writtenState << endl; 

	return writtenState;
}
/**
 * Since we need the bullets to collide with the walls in our world we choose to rock the location of the walls
 */
void Bullet::getWalls()
{
	rockvec = World::sInstance->Returnwalls();
	rocksvec = World::sInstance->ReturnRocks();


}
/**
 * This is a Reimplementation from our Player object
 */
bool Bullet::operator==(Bullet &other)
{
	// Game Object Part.
	//Call the == of the base, Player reference is
	//downcast explicitly.
	if(!GameObject::operator==(other)) return false;

	if(this->ECRS_AllState != other.ECRS_AllState) return false;

	if (!Maths::Is3DVectorEqual(this->mVelocity, other.mVelocity)) return false;
	if (!Maths::FP_EQUAL(this->mMaxLinearSpeed, other.mMaxLinearSpeed)) return false;
	if (!Maths::FP_EQUAL(this->mMaxRotationSpeed, other.mMaxRotationSpeed)) return false;
	if (!Maths::FP_EQUAL(this->mWallRestitution, other.mWallRestitution)) return false;
	if (!Maths::FP_EQUAL(this->mNPCRestitution, other.mNPCRestitution)) return false;
	if(this->mBulletId != other.mBulletId) return false;

	if (!Maths::FP_EQUAL(this->mLastMoveTimestamp, other.mLastMoveTimestamp)) return false;
	if (!Maths::FP_EQUAL(this->mThrustDir, other.mThrustDir)) return false;
	if(this->mHealth != other.mHealth) return false;
	if(this->mIsShooting != other.mIsShooting) return false;

	return true;
}
