#ifndef WORLD_H_
#define WORLD_H_
/*
* the world tracks all the live game objects. Failry inefficient for now, but not that much of a problem
*/

#include <memory>
#include <vector>


#include "GameObject.h"
#include "Rock.h"
#include "Rocks.h"

class World
{

public:

	static void StaticInit();

	static std::unique_ptr< World >		sInstance;

	void AddGameObject( GameObjectPtr inGameObject );
	void RemoveGameObject( GameObjectPtr inGameObject );

	int GetGameObjectCount();

	void Update();

	void  populatewalls();

	void  populateRocks();

	std::vector<Rock*>World::Returnwalls() { return rocklist; }

	std::vector<Rocks*>World::ReturnRocks() { return rockslist; }

	const std::vector< GameObjectPtr >&	GetGameObjects()	const	{ return mGameObjects; }

private:


	World();

	int	GetIndexOfGameObject( GameObjectPtr inGameObject );

	std::vector< GameObjectPtr >	mGameObjects;
	std::vector<Rock*>  rocklist;
	std::vector<Rocks*>  rockslist;

};

#endif // WORLD_H_
