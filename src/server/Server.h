#ifndef SERVER_H
#define SERVER_H

#include "Engine.h"
#include "ClientProxy.h"
#include "NetworkManagerServer.h"

#include "Bullet.h"
#include "Player.h"
#include "Rock.h"
#include "Rocks.h"

#include "RandGen.h"

#include <iostream>
using std::cout;
using std::endl;

class Server : public Engine
{
public:

	static bool StaticInit();

	virtual void DoFrame() override;

	virtual int Run();

	void HandleNewClient( ClientProxyPtr inClientProxy );
	void HandleLostClient( ClientProxyPtr inClientProxy );

	PlayerPtr	GetPlayer( int inPlayerId );
	void	SpawnPlayer( int inPlayerId );

	BulletPtr GetBullet(int BulletID);
	//void  SpawnBullet(int bulletID);


	//RockPtr	GetRock(int inRockId);
	void	SpawnRock(int inRockId, Vector3 spawnpos);

	void	SpawnRocks(int inRockId, Vector3 spawnpos);

private:
	Server();

	bool	InitNetworkManager();
	void	SetupWorld();

	RandGen* randGen;

	Vector3 getRandomSpawn();

	vector<Vector3> spawnPoints;

	Vector3 spawn0 = Vector3(1.0f, 3.0f, 0.0f);
	Vector3 spawn1 = Vector3(3.0f, 2.0f, 0.0f);
	Vector3 spawn2 = Vector3(2.25f,2.25f, 0.0f);
	Vector3 spawn3 = Vector3(1.0f, 1.25f, 0.0f);
	Vector3 spawn4 = Vector3(2.0f, 1.0f, 0.0f);

};

#endif // SERVER_H
