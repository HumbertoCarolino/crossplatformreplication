#pragma once

#include "Rocks.h"
#include "NetworkManagerServer.h"

class RocksServer : public Rocks
{
public:
	static GameObjectPtr	StaticCreate() { return NetworkManagerServer::sInstance->RegisterAndReturn(new RocksServer()); }
	//virtual void HandleDying() override;

	virtual void Update() override;

	

protected:
	RocksServer();

private:


};
