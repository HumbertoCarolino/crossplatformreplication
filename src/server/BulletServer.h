#ifndef BULLET_SERVER_H
#define BULLET_SERVER_H

#include "Bullet.h"
#include "NetworkManagerServer.h"

/*enum EBulletControlType
{
	ESCT_Human,
	ESCT_AI
};*/

class BulletServer : public Bullet
{
public:
	static GameObjectPtr	StaticCreate() { return NetworkManagerServer::sInstance->RegisterAndReturn( new BulletServer() ); }
	virtual void HandleDying() override;

	virtual void Update() override;


	//Not needed for the bullet
	//void SetControlType( EBulletControlType inPlayerControlType ) { mBulletControlType = inPlayerControlType; }


	//Bullet obviously doesnt take damage
	//void TakeDamage( int inDamagingPlayerId );

protected:
	BulletServer();

private:

	//void HandleShooting();

	//EBulletControlType	mBulletControlType;


	float		mTimeOfNextShot;
	float		mTimeBetweenShots;

};

#endif // PLAYER_SERVER_H
