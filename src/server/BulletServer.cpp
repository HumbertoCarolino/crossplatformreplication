#include "BulletServer.h"
#include "ClientProxy.h"
#include "Timing.h"
#include "MoveList.h"
#include "Maths.h"

BulletServer::BulletServer() :
	//mBulletControlType(ESCT_Human),
	mTimeOfNextShot( 0.f ),
	mTimeBetweenShots( 0.2f )
{}

void BulletServer::HandleDying()
{
	NetworkManagerServer::sInstance->UnregisterGameObject( this );
}

/**
 * Just like the player and Rock we need to update the object in the server since its not a static it need to be updated
 */
void BulletServer::Update()
{
	Bullet::Update();

	
	Vector3 oldLocation = GetLocation();
	Vector3 oldVelocity = GetVelocity();
	float oldRotation = GetRotation();

	/*ClientProxyPtr client = NetworkManagerServer::sInstance->GetClientProxy(GetBulletId());
	if( client )
	{
		//MoveList& moveList = client->GetUnprocessedMoveList();
		for (const Move& unprocessedMove : moveList)
		{
			const InputState& currentState = unprocessedMove.GetInputState();


		}
		//ProcessInput(deltaTime, currentState);
		
		//moveList.Clear();
	}*/

	float deltaTime = Timing::sInstance.GetDeltaTime();
	SimulateMovement(deltaTime);

	//HandleShooting();

	if( !Maths::Is2DVectorEqual( oldLocation, GetLocation() ) ||
		!Maths::Is2DVectorEqual( oldVelocity, GetVelocity() ) ||
		oldRotation != GetRotation() )
	{
		NetworkManagerServer::sInstance->SetStateDirty( GetNetworkId(), ECRS_Pose );
	}
}

/*void BulletServer::HandleShooting()
{
	float time = Timing::sInstance.GetFrameStartTime();
	if( mIsShooting && Timing::sInstance.GetFrameStartTime() > mTimeOfNextShot )
	{
		//not exact, but okay
		mTimeOfNextShot = time + mTimeBetweenShots;

		//fire!
	//	YarnPtr yarn = std::static_pointer_cast< Yarn >( GameObjectRegistry::sInstance->CreateGameObject( 'YARN' ) );
	//	yarn->InitFromShooter( this );
	}
}*/

/*void BulletServer::TakeDamage(int inDamagingPlayerId)
{
	mHealth--;
	if( mHealth <= 0.f )
	{
		//score one for damaging player...
		//ScoreBoardManager::sInstance->IncScore( inDamagingPlayerId, 1 );

		//and you want to die
		SetDoesWantToDie( true );

		//tell the client proxy to make you a new cat
		ClientProxyPtr clientProxy = NetworkManagerServer::sInstance->GetClientProxy( GetBulletId() );
		if( clientProxy )
		{
			clientProxy->HandlePlayerDied();
		}
	}

	//tell the world our health dropped
	NetworkManagerServer::sInstance->SetStateDirty( GetNetworkId(), ECRS_Health );
}*/
