
#include "Server.h"
#include "GameObjectRegistry.h"
#include "StringUtils.h"
#include "Colors.h"

#include "PlayerServer.h"
#include "BulletServer.h"
#include "RockServer.h"
#include "RocksServer.h"
#include <vector>

using std::vector;
using std::static_pointer_cast;

bool Server::StaticInit()
{
	sInstance.reset( new Server() );

	return true;
}
/**
 * Constructor for our sarver we also pass into our server all the objects(each one of our object scripts) we have inthe world 
 */
Server::Server()
{

	GameObjectRegistry::sInstance->RegisterCreationFunction( 'PLYR', PlayerServer::StaticCreate );
	GameObjectRegistry::sInstance->RegisterCreationFunction('BLET', BulletServer::StaticCreate);


	GameObjectRegistry::sInstance->RegisterCreationFunction('RCKO', RockServer::StaticCreate);
	GameObjectRegistry::sInstance->RegisterCreationFunction('RCKS', RocksServer::StaticCreate);

	InitNetworkManager();

	// Setup latency
	float latency = 0.0f;
	string latencyString = StringUtils::GetCommandLineArg( 2 );
	if( !latencyString.empty() )
	{
		latency = stof( latencyString );
	}
	NetworkManagerServer::sInstance->SetSimulatedLatency( latency );
}


int Server::Run()
{
	SetupWorld();

	return Engine::Run();
}

bool Server::InitNetworkManager()
{
	string portString = StringUtils::GetCommandLineArg( 1 );
	uint16_t port = stoi( portString );

	return NetworkManagerServer::StaticInit( port );
}

/**
 * This is to set up our world objects as an example we have the Rocks and then we are also setting the spawn points in the world. 
 */
void Server::SetupWorld()
{
	// Setup RNG
	
	randGen = new RandGen();
	randGen->StaticInit();

	// Setup spawn points
	spawnPoints = {
		spawn0,
		spawn1,
		spawn2,
		spawn3,
		spawn4
	};


	// Static game objects and NPCs here.
	//Top wall
	SpawnRock(0, Vector3(-3.57f, -3.0f, -1.0f));
	SpawnRock(1, Vector3(2.10f, -3.0f, -1.0f));
	SpawnRock(2, Vector3(7.77f, -3.0f, -1.0f));


	//Bottom Wall
	SpawnRock(3, Vector3(-3.57f, 3.0f, -1.0f));
	SpawnRock(4, Vector3(2.10f, 3.0f, -1.0f));
	SpawnRock(5, Vector3(7.77f, 3.0f, -1.0f));


	SpawnRocks(1, Vector3(.0f, .0f, -1.0f));
	SpawnRocks(2, Vector3(3.0f, 1.0f, -1.0f));
	SpawnRocks(3, Vector3(5.0f, 2.0f, -1.0f));
	SpawnRocks(4, Vector3(2.0f, 2.25f, -1.0f));
	SpawnRocks(5, Vector3(-3.0f, -1.0f, -1.0f));
	SpawnRocks(6, Vector3(-2.50f, -2.0f, -1.0f));
	SpawnRocks(7, Vector3(-2.0f, -2.25f, -1.0f));
	SpawnRocks(6, Vector3(2.50f, -2.0f, -1.0f));
	SpawnRocks(7, Vector3(-2.0f, 2.25f, -1.0f));


	World::sInstance->populatewalls();
	World::sInstance->populateRocks();


}
/**
 * This function is used to make our players Spawn at specific points but in a randomly order
 */
Vector3 Server::getRandomSpawn()
{
	int index = randGen->GetRandomInt(0, spawnPoints.size() - 1);
	Vector3 spawnPoint = spawnPoints[index];
	cout << "Spawn Index: " << index << endl << "Location: " << spawnPoint.mX << ", " << spawnPoint.mY << ", " << spawnPoint.mZ << endl;
	return spawnPoint;
}
/**
 * As the name sugests is a function to do each one of these functions on each frame.
 */
void Server::DoFrame()
{
	NetworkManagerServer::sInstance->ProcessIncomingPackets();

	NetworkManagerServer::sInstance->CheckForDisconnects();

	NetworkManagerServer::sInstance->RespawnPlayers();

	Engine::DoFrame();

	NetworkManagerServer::sInstance->SendOutgoingPackets();
}

void Server::HandleNewClient( ClientProxyPtr inClientProxy )
{

	int playerId = inClientProxy->GetPlayerId();

	//cout << "Player ID: " << playerId << endl; 

	//ScoreBoardManager::sInstance->AddEntry( playerId, inClientProxy->GetName() );
	SpawnPlayer( playerId );


}	
/**
 * This function is to Spawn our player into the game world it also sets his specific location into the world, and ID is used for each one of the players in our world
 */
void Server::SpawnPlayer( int inPlayerId )
{
	PlayerPtr player = std::static_pointer_cast< Player >( GameObjectRegistry::sInstance->CreateGameObject( 'PLYR' ) );
	player->SetColor( Colors::Red );//ScoreBoardManager::sInstance->GetEntry( inPlayerId )->GetColor() );
	player->SetPlayerId( inPlayerId );
	//chaged spawnner but needs refinement 
	player->SetLocation(getRandomSpawn());
	//player->SetLocation( Vector3( 1.f - static_cast< float >( inPlayerId ), 0.f, 0.f ) );
	//Vector3 playerpos = player->GetLocation();


	//cout << "Spawn Index: " << player

}

void Server::HandleLostClient( ClientProxyPtr inClientProxy )
{
	//kill client's player
	//remove client from scoreboard
	int playerId = inClientProxy->GetPlayerId();

	//ScoreBoardManager::sInstance->RemoveEntry( playerId );
	PlayerPtr player = GetPlayer( playerId );
	if( player )
	{
		player->SetDoesWantToDie( true );
	}
}


/**
 * In here we are getting the regestry to our player 
 */
PlayerPtr Server::GetPlayer( int inPlayerId )
{
	//run through the objects till we find the Player...
	//it would be nice if we kept a pointer to the Player on the clientproxy
	//but then we'd have to clean it up when the Player died, etc.
	//this will work for now until it's a perf issue
	const auto& gameObjects = World::sInstance->GetGameObjects();
	for( int i = 0, c = gameObjects.size(); i < c; ++i )
	{
		GameObjectPtr go = gameObjects[ i ];

		/* Original code did this in a weird way, used a method in the base (GameObject) which
		returned a player object if a game object is the player and null otherwise */

		uint32_t type = go->GetClassId();

		//Player* player = dynamic_cast<Player*>(*go);
		PlayerPtr player = nullptr;
		if(type == 'PLYR')
		{
			player = std::static_pointer_cast< Player >(go);
		}

		if(player && player->GetPlayerId() == inPlayerId )
		{
			return player;
		}
	}

	return nullptr;

}

/**
 * In here we are doing the same as we do with the player
 */
BulletPtr Server::GetBullet(int BulletID)
{


	const auto& gameObjects = World::sInstance->GetGameObjects();
	for (int i = 0, c = gameObjects.size(); i < c; ++i)
	{
		GameObjectPtr go = gameObjects[i];

		/* Original code did this in a weird way, used a method in the base (GameObject) which
		returned a player object if a game object is the player and null otherwise */

		uint32_t type = go->GetClassId();

		//Player* player = dynamic_cast<Player*>(*go);
		BulletPtr bullet = nullptr;
		if (type == 'BLET')
		{
			bullet = std::static_pointer_cast<Bullet>(go);
		}

		if (bullet && bullet->GetBulletId() == BulletID)
		{
			return bullet;
		}
	}

	return nullptr;
}



//RockPtr Server::GetRock(int inRockId)
//{
//
//	const auto& gameObjects = World::sInstance->GetGameObjects();
//	for (int i = 0, c = gameObjects.size(); i < c; ++i)
//	{
//		GameObjectPtr go = gameObjects[i];
//
//		/* Original code did this in a weird way, used a method in the base (GameObject) which
//		returned a player object if a game object is the player and null otherwise */
//
//		uint32_t type = go->GetClassId();
//
//		//Player* player = dynamic_cast<Player*>(*go);
//		RockPtr rock = nullptr;
//		if (type == 'RCKO')
//		{
//			rock = std::static_pointer_cast<Rock>(go);
//		}
//
//	
//	}
//
//
//
//	return nullptr;
//}


/**
 * Function to spawn  our Rock which is our static object in the world. We dont need the ID for Rock
 */
void Server::SpawnRock(int inRockId, Vector3 spawnpos)
{

	RockPtr rock = std::static_pointer_cast<Rock>(GameObjectRegistry::sInstance->CreateGameObject('RCKO'));
	rock->SetColor(Colors::Red);

	//rock->SetRockId(inRockId);

	rock->SetLocation(spawnpos);

	cout << "Spawn Index: " << spawnpos.mX << ", " << spawnpos.mY << ", " << spawnpos.mZ << endl;
	


}

void Server::SpawnRocks(int inRockId, Vector3 spawnpos)
{
	RocksPtr rocks = std::static_pointer_cast<Rocks>(GameObjectRegistry::sInstance->CreateGameObject('RCKS'));
	rocks->SetColor(Colors::Red);

	//rock->SetRockId(inRockId);

	rocks->SetLocation(spawnpos);

	cout << "Spawn Index: " << spawnpos.mX << ", " << spawnpos.mY << ", " << spawnpos.mZ << endl;

}
