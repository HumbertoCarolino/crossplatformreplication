#include <limits.h>
#include <math.h>
#include "gtest/gtest.h"

#include "RockTestHarness.h"
#include "Rock.h"
#include "RockClient.h"
#include "TextureManager.h"
#include "Maths.h"
#include "Colors.h"

#include "InputMemoryBitStream.h"
#include "OutputMemoryBitStream.h"

#include <iostream>
#include <fstream>
#include <thread>

/* Reference: http://www.yolinux.com/TUTORIALS/Cpp-GoogleTest.html */

RockTestHarness::RockTestHarness()
{
  pp = nullptr;
}

RockTestHarness::~RockTestHarness()
{
  pp.reset();
}

void RockTestHarness::SetUp()
{
    GameObject*	go = Rock::StaticCreate();
    Rock* p = static_cast<Rock*>(go);
    this->pp.reset(p);
}

void RockTestHarness::TearDown()
{
    this->pp.reset();
    this->pp = nullptr;
}

TEST_F(RockTestHarness,constructor_noArgs)
{
  // Check defaults are set
  // Should be no need to do these as they were tested with the base class.
  EXPECT_TRUE(Maths::Is3DVectorEqual(pp->GetColor(), Colors::White));
  EXPECT_TRUE(Maths::Is3DVectorEqual(pp->GetLocation(), Vector3::Zero));
  EXPECT_FLOAT_EQ(pp->GetCollisionRadius(), 0.5f);
  EXPECT_FLOAT_EQ(pp->GetScale(),1.0f);
  EXPECT_FLOAT_EQ(pp->GetRotation(),0.0f);
  EXPECT_EQ(pp->GetIndexInWorld(), -1);
  EXPECT_EQ(pp->GetNetworkId(), 0);

  //Initial state is update all
  int check = 0x000F; //Hex - binary 00000000 00000000 00000000 00001111
  EXPECT_EQ(pp->GetAllStateMask(), check);

  //Check our macro has worked.
  EXPECT_EQ(pp->GetClassId(), 'RCKO');
  EXPECT_NE(pp->GetClassId(), 'HELP');
}


/* Tests Omitted
* There's a good chunk of this which cannot be tested in this limited example,
* however there should be enough to underake some testing of the serialisation code.
*/

TEST_F(RockTestHarness,EqualsOperator1)
{ /* Won't compile - why?
  Player a ();
  Player b ();

  a.SetPlayerId(10);
  b.SetPlayerId(10);

  EXPECT_TRUE(a == b);*/
}

TEST_F(RockTestHarness,EqualsOperator2)
{
  Rock *a = static_cast<Rock*>(Rock::StaticCreate());
  Rock *b = static_cast<Rock*>(Rock::StaticCreate());

  a->SetRockId(10);
  b->SetRockId(10);

  EXPECT_TRUE(*a == *b);
}

/* Need more tests here */

TEST_F(RockTestHarness,EqualsOperator3)
{
  Rock *a = static_cast<Rock*>(Rock::StaticCreate());
  Rock *b = static_cast<Rock*>(Rock::StaticCreate());

  a->SetRockId(10);
  b->SetRockId(30);

  EXPECT_FALSE(*a == *b);
}

TEST_F(RockTestHarness,EqualsOperator4)
{
  RockPtr b(static_cast<Rock*>(Rock::StaticCreate()));

  pp->SetRockId(10);
  b->SetRockId(10);

  EXPECT_TRUE(*pp == *b);
}

/* Serialistion tests in MemoryBitStreamTestHarness */